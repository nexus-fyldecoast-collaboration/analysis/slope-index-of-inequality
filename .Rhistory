}
### Calculate the slopes
calculateSlopesFromTable <- function(linModTable){
### performs leat squares regression -
linest <- lm(val ~ 0 + SqrtA + bSqrtA, data=linModTable)
## beta coefficients for bSqrtA is the SII
thisSII <- coef(linest)[2]
### (SqrtA + bSqrtA)/SqrtA is the RII
thisRII <- (coef(linest)[1] + coef(linest)[2])/coef(linest)[1]
### Return both in a single row dataframe
return(data.frame(SII=thisSII, RII=thisRII))
}
### Simulate DSRs, using the standard error
simulateDsrs <- function(Dsrs, DerivedSE, numberOfSimulations = 10000){
### Create an empty list
simulatedDSRs <- list()
### cycle through all the ntiles
for(tile in 1:ntiles){
### generate x normally distributed random variables, with
### the rate as the mean, and the standard error as the standard deviation
simulatedDSRs[[tile]] <- rnorm(numberOfSimulations, Dsrs$Std_Rate[tile], DerivedSE[tile])
}
### Column bind all the generated DSRs into a dataframe
simulatedDSRs <- do.call(cbind, simulatedDSRs)
return(simulatedDSRs)
}
### Function to simulate confidence intervals for the slopes
simulateConfidenceIntervals <- function(simulatedDSRs, linModTable, numberOfSimulations){
### create empty list to append to
allSiiSimulations <- list()
### Cycle through all the simulations of the DSRs and calculate
### the slopes for each one
for (j in 1:numberOfSimulations){
linModTable$val <- simulatedDSRs[j,] * linModTable$SqrtA
allSiiSimulations[[j]] <- calculateSlopesFromTable(linModTable)
}
### row bind all the items in the list
allSimulationsDf <- do.call(rbind, allSiiSimulations)
### Cycle through all the columns in the table
### which are the different slope calculations (e.g. SII, RII)
for(i in 1:ncol(allSimulationsDf)){
## Get all the slopes for slope type
allSimulatedSlopes <- allSimulationsDf[,i]
## Get the standard deviation
simulationSd <- sd(allSimulatedSlopes)
## Apply the standard deviation of the slopes, as the standard
## error to create 95% confidence intervals
thisLCL <-  mean(allSimulatedSlopes) - (simulationSd * 1.96)
thisUCL <-  mean(allSimulatedSlopes) + (simulationSd * 1.96)
### Calculate the z value
thisZval <- abs(mean(allSimulatedSlopes) / simulationSd)
### Use this to calculate a p-value
### This is a bit of a fudge (https://www.bmj.com/content/343/bmj.d2304)
thisPval <- exp(-0.717*thisZval -0.416*thisZval^2) %>%
pmin(1)
### Get the name of this slope type from the column name from the table
thisSlopeName <- names(allSimulationsDf)[i]
### Put values in a single row dataframe
valsInPass <- data.frame(LCL = thisLCL,
UCL = thisUCL,
Pval = thisPval)
### Add in the slope type to the column names
names(valsInPass) =  paste(thisSlopeName, names(valsInPass),sep="_")
### Add all coulumns from all slope types into the allCis datafrape
if(i == 1) {
allCis <- valsInPass
} else {
allCis <- cbind(allCis, valsInPass)
}
}
return(allCis)
}
### Create vectors with all of everything to be used
usedLsoaCodes <- as.character(unique(finalPops$`Area Codes`))
usedSex <- c("M", "F")
usedEventType <- c("Non-Elective Admission", "Death")
usedQuintiles <- 1:ntiles
usedLastAgeband <- 100
allIcd10Codes <- as.character(unique(allEvents$ICD10))
### Define the codes ICD 10 categories to be used
uniCodes1 <- unique(Icd10Table[,c("ICD10Category1Code", "ICD10Category1")])
names(uniCodes1) <- c("code", "name")
uniCodes2 <- unique(Icd10Table[,c("ICD10Category2Code", "ICD10Category2")])
names(uniCodes2) <- c("code", "name")
uniCodes3 <- unique(Icd10Table[,c("ICD10Category3Code", "ICD10Category3")])
names(uniCodes3) <- c("code", "name")
### Get all unique ones from all levels
uniCodes <- uniCodes1
uniCodes$Lev <- 1
uniCodes <- rbind(uniCodes,
data.frame(uniCodes2[!(uniCodes2$code %in% uniCodes1$code),],Lev = 2))
uniCodes <- rbind(uniCodes,
data.frame(uniCodes3[!(uniCodes3$code %in% uniCodes$code),],Lev = 3))
### Get all the chapters
chapters <- unique(Icd10Table[,c("ICD10ChapterCode", "ICD10ChapterDescription")])
chapters <- chapters[chapters$ICD10ChapterCode != "-",]
for(i in 1:nrow(chapters)){
thisChapCodes <- Icd10Table$ICD10Code[Icd10Table$ICD10ChapterCode == chapters[i,]$ICD10ChapterCode]
thisChapCodes <- as.character(thisChapCodes) %>%
substr(1,3) %>%
unique
uniCodes <- rbind(uniCodes,
data.frame(
code=paste(thisChapCodes[1],thisChapCodes[length(thisChapCodes)],sep="-"),
name=chapters[i,]$ICD10ChapterDescription,
Lev=0
)
)
}
includeAll3Digit <- TRUE ### This adds about 1800 categories... probably best to run this overnight if true
if(includeAll3Digit){
## This does all 3 digit
uniCodes3Dig <- unique(Icd10Table[nchar(as.character(Icd10Table$ICD10Code)) == 3,c("ICD10Code", "ICD10Description")])
names(uniCodes3Dig) <- c("code", "name")
uniCodes3Dig$code <- paste(uniCodes3Dig$code, uniCodes3Dig$code, sep="-")
uniCodes3Dig$Lev <- 4
uniCodes <- rbind(uniCodes, uniCodes3Dig)
}
### Create a list which contains all the chapter/block names and their ranges
ICD10Groups <- list()
for(i in 1:nrow(uniCodes)){
thisGroup <- uniCodes[i,]
startAndEnd <- c(gsub( "-.*$", "", thisGroup$code ), sub(".*\\-", "", thisGroup$code))
names(startAndEnd) <- c("start", "end")
ICD10Groups[[paste(thisGroup$code, thisGroup$name, sep = " - ")]] <- startAndEnd
}
### Delete group with dashes
ICD10Groups <- ICD10Groups[substr(names(ICD10Groups),1,1) != "-"]
### Create empty data frames to contain all the DSRs and the slopes
allDsrs <- data.frame()
allSlopes <- data.frame()
ICD10Groups
View(ICD10Groups)
str(ICD10Groups)
tst <- do.call(rbind, ICD10Groups)
which(tst == "T40")
tst <- do.call(rbind, ICD10Groups)
chaptInd = which(tst == "T40")[2]
### Get the range and name of the chapter in this pass
thisChapt <- ICD10Groups[[chaptInd]]
thisChaptName <- names(ICD10Groups[chaptInd])
### Get a subset of allIcd10Codes where within the range of the chapter
usedIcd10Codes <- allIcd10Codes[substr(as.character(allIcd10Codes),1,nchar(thisChapt[1])) >= thisChapt[1] &
substr(as.character(allIcd10Codes),1,nchar(thisChapt[1])) <= thisChapt[2]]
chaptInd
tst
tst <- do.call(rbind, ICD10Groups)
chaptInd = which(tst == "T40")[1]
thisChapt <- ICD10Groups[[chaptInd]]
thisChapt
thisChaptName <- names(ICD10Groups[chaptInd])
### Get a subset of allIcd10Codes where within the range of the chapter
usedIcd10Codes <- allIcd10Codes[substr(as.character(allIcd10Codes),1,nchar(thisChapt[1])) >= thisChapt[1] &
substr(as.character(allIcd10Codes),1,nchar(thisChapt[1])) <= thisChapt[2]]
usedIcd10Codes
print(length(usedIcd10Codes))
### calculate the DSRs using the usedIcd10Codes subset
Dsrs <- calculateDSR(events = allEvents,
pop = finalPops,
stdpop = usedStpPop,
usedLsoaCodes,
usedSex,
usedIcd10Codes,
usedEventType,
usedQuintiles ,
usedLastAgeband)
### Give the output sensible names
names(Dsrs) <- c("Subgroup",
"Numerator",
"Denominator",
"Crude_Rate",
"Crude_Rate_LCL",
"Crude_Rate_UCL",
"Std_Rate",
"Std_LCL",
"Std_UCL")
Dsrs
print(nrow(Dsrs))
sum(as.numeric(Dsrs$Numerator == 0)) == 0
## Calculate standard error from confidence intervals
DerivedSE <- (Dsrs$Std_UCL - Dsrs$Std_LCL)/1.96/2
### Create table used for SII and RII calculation
linModTable <- createTableForLm(Dsrs, DerivedSE)
### Calculate slopes
calculatedSlopes <- calculateSlopesFromTable(linModTable)
numSimulations <- 10000
### Generate table of simulated DSRs, based on the standard error
simulatedDSRs <- simulateDsrs(Dsrs, DerivedSE, numSimulations)
### Calculate simulated confidence intervals from the simulated DSRs
simCis <- simulateConfidenceIntervals(simulatedDSRs, linModTable, numSimulations)
### Put the slopes and CIs in a single row dataframe
thisPassSlopes <- data.frame(name = thisChaptName, calculatedSlopes, simCis)
### Append to all slopes
allSlopes <- rbind(allSlopes, thisPassSlopes)
allSlopes
calculatedSlopes
linest <- lm(val ~ 0 + SqrtA + bSqrtA, data=linModTable)
linest
thisSII <- coef(linest)[2]
thisSII
thisRII <- (coef(linest)[1] + coef(linest)[2])/coef(linest)[1]
thisRII
linest
(coef(linest)[1] + coef(linest)[2])
coef(linest)[1]
coef(linest)[2]
bCoef <- coef(linest)[2]
intercept <- coef(linest)[1]
thisSII <- bCoef
### (SqrtA + bSqrtA)/SqrtA is the RII
thisRII <- (intercept + bCoef)/intercept
thisRII
thisSII
intercept <- 10e-16
intercept
intercept <- 1e-16
if(intercept <= 0){
intercept <- 1e-16
linest <- lm(I(val - intercept) ~ SqrtA + bSqrtA, data=linModTable)
}
bCoef <- coef(linest)[2]
bCoef
thisSII <- bCoef
### (SqrtA + bSqrtA)/SqrtA is the RII
thisRII <- (intercept + bCoef)/intercept
thisRII
SqrtA
linModTable
intercept <- min(linModTable$val) #1e-16
linest <- lm(I(val - intercept) ~ SqrtA + bSqrtA, data=linModTable)
bCoef <- coef(linest)[2]
intercept
thisSII <- bCoef
thisSII
linest
linest <- lm(val ~ 0 + SqrtA + bSqrtA, data=linModTable)
linest
linest <- lm(I(val - intercept) ~ SqrtA + bSqrtA, data=linModTable)
bCoef <- coef(linest)[2]
bCoef
linest <- lm(val ~ 0 + SqrtA + bSqrtA, data=linModTable)
## beta coefficients for bSqrtA is the SII
bCoef <- coef(linest)[2]
bCoef
bCoef <- coef(linest)[3]
bCoef
coef(linest)[1]
coef(linest)[2]
bCoef <- coef(linest)[2]
thisRII <- (intercept + bCoef)/intercept
thisRII
linest
calculatedSlopes
rm(list=ls())
### I need to set this because my machine is buttoned down
.libPaths('F:/R/win-library/3.5')
### Set the directory here
setwd("F:/slope-index-of-inequality")
### Load all the required libraries
if(!require(readxl)) {
install.packages("readxl")
library(readxl)
}
if(!require(tidyverse)) {
install.packages("tidyverse")
library(tidyverse)
}
if(!require(RODBC)) {
install.packages("RODBC")
library(RODBC)
}
if(!require(reshape2)) {
install.packages("reshape2")
library(reshape2)
}
if(!require(icd.data)) {
install.packages("icd.data")
library(icd.data)
}
if(!require(dsr)) {
install.packages("dsr")
library(dsr)
}
library(readxl)
library(tidyverse)
library(RODBC)
library(reshape2)
library(icd.data)
library(dsr)
### Set data source for events - Change 1 to 2 for mortality
CalcType <- c("Non-elective Admissions", "Mortality")[1]
### Set the number tiles... i.e 5 for quintile, 10 for decile
ntiles <-  5
### Folder locations and variables for population estimates
popEstimatesfolder <- "Data/LsoaPopulationEstimates/"
popEstimatesFiles <- popEstimatesfolder %>%
list.files
### Get a list of all the years we have population estimates for
popEstimatesYears <-  popEstimatesFiles %>%
tolower  %>%
gsub("sape20dt1-mid-", "" ,.) %>%
gsub("-lsoa-syoa-estimates-formatted.xls", "" ,.) %>%
as.numeric
genders <- c("Males", "Females")
### Folder locations and variables for IMD
imdfolder <- "Data/IMD/"
### list the files
imdFile <- imdfolder %>%
list.files
### Folder location and read in of a CSV which lists the used LAs and LSOAs
usedLaFile <- "Settings/UsedLAs.CSV"
usedLaTable <- read.csv(usedLaFile, header=TRUE)
usedLsoaFile <- "Settings/UsedLSOAsAdditionals.CSV"
usedLsoaTable <- read.csv(usedLsoaFile, header=TRUE)
### Folder location and read in of a ICD10 codes and chapters
usedICD10File <- "Data/ICD10/ICD10_Ref.csv"
Icd10Table <- read.csv(usedICD10File, header=TRUE)
### Specify start and end years
startYear <- 2014
endYear <- 2018
yearsArray <- startYear:endYear
### Load Standard Population (2013 ESP)
usedStandardPopulationFile <- "Data/StandardPopulation/european_standard_population_by_sex.csv"
StandardPopTable <- read.csv(usedStandardPopulationFile, header=TRUE)
### Set the age at the start of the ageband (e.g. 40 for 40-44) to make it wasy for linking
startAges <- substr(StandardPopTable$AgeGroup,1,2)
startAges <- as.numeric(gsub("[^0-9]", "", startAges)) # removes non-numeric characters
### Put neatly into a data frame
usedStpPop <- data.frame(age=startAges,
sex=substr(StandardPopTable$Sex,1,1),
pop=StandardPopTable$EuropeanStandardPopulation)
### Get a vector of unique start ages
startAges <- unique(startAges)
### Get max age from standard population - we can cap all the data here
maxAgeUsed <- max(startAges)
### Create lookup from single year age to agebands used un the standard population
ageToAgeband <- data.frame(age = 0:maxAgeUsed)
for(i in 1:nrow(ageToAgeband)){
ageToAgeband$ageBand[i] <- startAges[
findInterval(i, startAges+1, left.open = FALSE)]
}
### Get IMD from excel files
### Available from https://www.gov.uk/government/statistics/english-indices-of-deprivation-2015
### https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/467764/File_1_ID_2015_Index_of_Multiple_Deprivation.xlsx
imd <- imdfolder %>%
paste(imdFile, sep="" )  %>%
read_excel(sheet = "IMD 2015")
#### Filter the IMD on the UsedLaTable (which has the used LAs), then use this as the reference for all LSOAs
imdFiltered <- imd[imd$`Local Authority District code (2013)` %in% as.character(usedLaTable$Code) |
imd$`LSOA code (2011)` %in% as.character(usedLsoaTable$Lsoa),]
#### Create a local ntile based on that filtered data
imdFiltered$ntileLocal <-  imdFiltered$`Index of Multiple Deprivation (IMD) Rank (where 1 is most deprived)` %>%
rank()/(nrow(imdFiltered)/ntiles) %>%
ceiling
imdFiltered$ntileLocal <- ceiling(imdFiltered$ntileLocal)
#### Tidy up the format a bit
imdFilteredForMerge <- imdFiltered[,c("LSOA code (2011)","ntileLocal")]
names(imdFilteredForMerge)[1] <- "LSOA"
######## Get the population estimates from excel files ########
### set empty dataframe
allPops <- data.frame()
### Cycle through all population estimate excel filesfiles
### Available from "https://www.ons.gov.uk/peoplepopulationandcommunity/populationandmigration/populationestimates/datasets/lowersuperoutputareamidyearpopulationestimates"
### Cycle through years (as defined at the top)
for(yearInd in 1:length(yearsArray)){
### Cycle through genders as defined (as defined at the top)
for (genderInd in 1:length(genders)){
thisYear <- yearsArray[yearInd]
### Check to see if there is a pop estimate for the year
### if not, use first or last available closest to the year
if(thisYear %in% popEstimatesYears){
fileIndex <- which(popEstimatesYears == thisYear)
} else {
if(thisYear > max(popEstimatesYears)){
fileIndex <- length(popEstimatesYears)
} else {
fileIndex <- 1
}
}
thisGender <- genders[genderInd]
### Read in the file
thisPops <- popEstimatesfolder %>%
paste(popEstimatesFiles[fileIndex], sep="" )  %>%
read_excel(sheet = paste("Mid-",popEstimatesYears[fileIndex]," ",thisGender, sep=""), skip = 4)
### Populate year and gender as per this pass in the loop
thisPops$year <- thisYear
thisPops$gender <- thisGender
### The excel contains totals populations for the LAs - this removes them
thisPops <- thisPops[is.na(thisPops$`Area Names`),]
### Get rid of unused fields
thisPops$`Area Names` <- NULL
thisPops$`All Ages` <- NULL
### One of the columns in the excel isn't named, so it gets assigned "...3" - this gives it a sensible name
names(thisPops)[names(thisPops) == "...3"] <- "Area Names"
### Add the populations from this pass to the "allPops" dataframe
allPops <- rbind(allPops, thisPops)
}
}
### Filter to only LSOAs in the IMD data frame (filtered previously based on used LAs csv)
allPops <- allPops[allPops$`Area Codes` %in% imdFiltered$`LSOA code (2011)`,]
### Make long thin table, with ages as values in a col rather that cols themselves
finalPops <- allPops %>%
names %>%
substr(1, 1) %>%
grepl('[A-Za-z]',.) %>%
names(allPops)[.] %>%
melt(allPops, id = .)
#### rename col names to sensible ones
names(finalPops)[names(finalPops) == "variable"] <- "Age"
names(finalPops)[names(finalPops) == "value"] <- "Population"
### Remove the + from 90+ in the ages
finalPops$Age <- as.numeric(gsub("[^0-9]", "", finalPops$Age))
### Cap age at maxAgeUsed
finalPops$Age <- pmin(maxAgeUsed, finalPops$Age )
### Merge with ageband lookup - single age to agaband
finalPops <- merge(finalPops, ageToAgeband, by.x = "Age", by.y = "age", all.x = TRUE)
### Group by only important cols and use ageband and aggregate (sum) population
finalPops <- finalPops %>%
group_by(`Area Codes`,`gender`,`ageBand`) %>%
summarize(Pop = sum(Population))
### Link with the IMD table to get the deprivation decile
finalPops <- merge(finalPops, imdFilteredForMerge, by.x = "Area Codes", by.y = "LSOA", all.x = TRUE)
### Tidy up and remove unused dataframes
rm(thisPops)
rm(allPops)
######## Get admissions from MLCSU Database ########
### Concatenate text to make a query which uses years from above and cap at maxAgeUsed
susAdmissionsQuery <- paste("
SELECT CASE
WHEN [age] > ",maxAgeUsed," THEN ",maxAgeUsed,"
ELSE [age]
END                          [Age]
--,[DischargeFinancialYearNumber] As [Year]
,
'Non-Elective Admission'     AS [EventType],
LEFT([genderdescription], 1) AS [Sex],
[lsoacode]                   [LSOA]
--,[GPPracticeCode] AS [GP]
,
[primarydiagnosisicd10code]  AS [ICD10],
Count(*)                     AS [Events]
FROM   [SUS].[inpatientspell]
WHERE  [admissionmethodcode] IN( '21', '22', '23', '24',
'25', '28', '2A', '2B', '2D' )
--AND STPName = 'Lancashire and South Cumbria'
AND [lsoacode] != '---------'
AND dischargefinancialyearnumber >= ",startYear,"
AND dischargefinancialyearnumber < ",endYear,"
AND LEFT([genderdescription], 1) IN ( 'M', 'F' )
GROUP BY
CASE WHEN [age] > ",maxAgeUsed," THEN ",maxAgeUsed,"
ELSE [age]
END,
[dischargefinancialyearnumber],
[genderdescription],
[lsoacode],
[gppracticecode],
[primarydiagnosisicd10code]
")
susMortalityQuery <- paste("
SELECT CASE
WHEN [dec_sex] = '1' THEN 'M'
WHEN [dec_sex] = '2' THEN 'F'
END                                     AS [Sex],
'Death'                                 AS [EventType],
CASE
WHEN [age_at_death] > ",maxAgeUsed," THEN ",maxAgeUsed,"
ELSE [age_at_death]
END                                     AS [Age],
LEFT([s_underlying_cod_icd10] + 'X', 4) AS [ICD10],
[lsoa_of_residence_code]                AS [LSOA],
Count(*)                                AS [Events]
FROM   [LocalFeeds].[Reporting].[mortality_data]
WHERE  ( [county_district_of_res_code] = 'E06000008'
OR [county_district_of_res_code] = 'E06000009'
OR [county_district_of_res_code] = 'E07000027'
OR [county_of_residence_code] = 'E10000017' )
AND LEFT([date_of_registration], 4) BETWEEN
",startYear ," AND ",endYear ,"
AND [s_underlying_cod_icd10] IS NOT NULL
GROUP  BY [dec_sex],
CASE
WHEN [age_at_death] > ",maxAgeUsed," THEN ",maxAgeUsed,"
ELSE [age_at_death]
END,
[s_underlying_cod_icd10],
[lsoa_of_residence_code]
")
if(CalcType == "Mortality") {
usedQuery = susMortalityQuery
rateMultiplyer = 100000
healthEventType = "Mortality"
### Connection to CSU Database
mlcsu_conn_glob <-
odbcDriverConnect(
'driver={SQL Server};server=mlcsu-bi-sql-at;database=LocalFeeds;trusted_connection=true'
)
} else {
usedQuery = susAdmissionsQuery
rateMultiplyer = 1000
healthEventType = "Non-elective"
### Connection to CSU Database
mlcsu_conn_glob <-
odbcDriverConnect(
'driver={SQL Server};server=mlcsu-bi-sql;database=AnalystGlobal;trusted_connection=true'
)
}
#### Run query and put the results into a dataframe
allEvents <- sqlQuery(mlcsu_conn_glob, usedQuery)
### Merge with agaband lookup - single age to ageband
allEvents <- merge(allEvents, ageToAgeband, by.x = "Age", by.y = "age", all.x = TRUE)
### Group by only important cols and use ageband and aggregate (sum) events
